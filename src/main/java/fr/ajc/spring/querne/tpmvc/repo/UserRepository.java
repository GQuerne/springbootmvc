package fr.ajc.spring.querne.tpmvc.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import fr.ajc.spring.querne.tpmvc.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
	
	List<User> findAllByOrderByRegistrationAsc();
	List<User> findAllByOrderByRegistrationDesc();
	
	List<User> findAllByOrderByCompanyAsc();
	List<User> findAllByOrderByCompanyDesc();
	
	User findBySiret(String siret);
}
