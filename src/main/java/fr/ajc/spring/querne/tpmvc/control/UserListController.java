package fr.ajc.spring.querne.tpmvc.control;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.ajc.spring.querne.tpmvc.model.User;
import fr.ajc.spring.querne.tpmvc.model.UserExposed;
import fr.ajc.spring.querne.tpmvc.repo.UserRepository;

@RestController
@RequestMapping("/api/users")
public class UserListController {

	@Autowired
	private UserRepository userRepository;

	@GetMapping
	public ResponseEntity displayUsers(@RequestParam(value="sort", required=false) String sort, @RequestParam(value="order", required=false) String order) {
		List<User> users =  userRepository.findAll();

		if (users.size() == 0) {
			return new ResponseEntity("No users found", HttpStatus.NOT_FOUND);
		} else {
			List<UserExposed> display = new ArrayList<UserExposed>();

			if (sort != null) {
				switch (sort) {
				case "byDate":
					if (order.equals("DESC")) {
						users = userRepository.findAllByOrderByRegistrationDesc();
					} else {
						users = userRepository.findAllByOrderByRegistrationAsc();
					}
					break;
				case "byCompany":
					if (order.equals("DESC")) {
						users= userRepository.findAllByOrderByCompanyDesc();
					} else {
						users = userRepository.findAllByOrderByCompanyAsc();
					}
					break;
				}
			}

			for (User user : users) {
				UserExposed userE = new UserExposed(user);
				display.add(userE);
			}		
			return new ResponseEntity(display, HttpStatus.OK);
		}
	}

	//display User Find By Siret
	public User findBySiret (String siret) {
		return userRepository.findBySiret(siret);
	}
	@GetMapping("/{siret}")
	public ResponseEntity displayUserFindBySiret(@PathVariable String siret) {
		User user =  findBySiret(siret);
		if (user == null) {
			return new ResponseEntity("No user found with siret="+siret, HttpStatus.NOT_FOUND);
		}
		UserExposed userE = new UserExposed(user);	
		return new ResponseEntity(userE, HttpStatus.OK);
	}

}
