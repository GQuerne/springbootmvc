package fr.ajc.spring.querne.tpmvc.model;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Optional;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

@Entity
public class UserExposed {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@NotEmpty
	String company;
	@Valid
	String tva;
	@NotEmpty
	@Valid
	String siret;
	@Valid
	@Min(5)
	int employees;
	boolean newsletter;
	LocalDateTime registration;
	
	public UserExposed() {
	}
	
	public UserExposed(User user) {
		this.id = user.getId();
		this.company = user.getCompany();
		this.tva = user.getTva();
		this.siret = user.getSiret();
		this.employees = user.getEmployees();
		this.newsletter = user.isNewsletter();
		this.registration = user.getRegistration();
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}

	public String getTva() {
		return tva;
	}
	public void setTva(String tva) {
		this.tva = tva;
	}

	public String getSiret() {
		return siret;
	}
	public void setSiret(String siret) {
		this.siret = siret;
	}
	public int getEmployees() {
		return employees;
	}
	public void setEmployees(int employees) {
		this.employees = employees;
	}
	public boolean isNewsletter() {
		return newsletter;
	}
	public void setNewsletter(boolean newsletter) {
		this.newsletter = newsletter;
	}

	public LocalDateTime getRegistration() {
		return registration;
	}
	public void setRegistration(LocalDateTime registration) {
		this.registration = registration;
	}

	@Override
	public String toString() {
		return "UserExposed [id=" + id + ", company=" + company + ", tva=" + tva + ", siret=" + siret + ", employees="
				+ employees + ", newsletter=" + newsletter + ", registration=" + registration + "]";
	}
}
