package fr.ajc.spring.querne.tpmvc.model;

import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

@Entity
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@NotEmpty
	@Pattern(regexp = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])")
	String email;
	//regexp 5322
	@NotEmpty
	@Pattern(regexp = "^(?=.*\\d)(?=.*[A-Z])(?=.*[a-z])([^\\s]){6,}$")
	String pass;
	//regexp min 6 char, at least 1 uppercase, 1 lowercase, 1digit
	@NotEmpty
	String company;
	@Pattern(regexp="^FR(\\d{11})$")
	String tva;
	@NotEmpty
	@Pattern(regexp="^\\d{14}$")
	String siret;
	@Valid
	@Min(5)
	int employees;
	boolean newsletter;
	LocalDateTime registration;
	
	public User() {
		this.registration = LocalDateTime.now();
	}

	public long getId() {
		return id;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}

	public String getTva() {
		return tva;
	}
	public void setTva(String tva) {
		this.tva = tva;
	}

	public String getSiret() {
		return siret;
	}
	public void setSiret(String siret) {
		this.siret = siret;
	}

	public int getEmployees() {
		return employees;
	}
	public void setEmployees(int employees) {
		this.employees = employees;
	}

	public boolean isNewsletter() {
		return newsletter;
	}
	public void setNewsletter(boolean newsletter) {
		this.newsletter = newsletter;
	}

	public LocalDateTime getRegistration() {
		return registration;
	}
	public void setRegistration(LocalDateTime registration) {
		this.registration = registration;
	}

	@Override
	public String toString() {
		return "UserModel [id=" + id + ", email=" + email + ", pass=" + pass + ", socialReason=" + company
				+ ", tva=" + tva + ", siret=" + siret + ", employees=" + employees + ", newsletter=" + newsletter
				+ ", registration=" + registration + "]";
	}
	
}
