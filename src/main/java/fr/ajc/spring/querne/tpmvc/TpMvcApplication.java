package fr.ajc.spring.querne.tpmvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TpMvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(TpMvcApplication.class, args);
	}

}
