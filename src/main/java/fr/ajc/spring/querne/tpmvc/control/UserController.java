package fr.ajc.spring.querne.tpmvc.control;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import fr.ajc.spring.querne.tpmvc.model.User;
import fr.ajc.spring.querne.tpmvc.repo.UserRepository;


@Controller
public class UserController implements WebMvcConfigurer{

	@Autowired
	private UserRepository userRepository;
	
	@Value("${spring.application.name}")
	String appName;

	//Accueil
	@GetMapping("/")
	public String homePage(Model model) {
		model.addAttribute("appName", appName);
		model.addAttribute("user", new User());
		return "home";
	}

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/success").setViewName("success");
	}

	//creation de compte
	@GetMapping("/createUserAccount")
	public String showForm(User user) {
		return "formUser";
	}

	//compte créé
	@PostMapping("/createUserAccount")
	public String checkUserValidation(@Valid@ModelAttribute("user") User user, BindingResult bindingResult, HttpServletResponse response) {
		if (bindingResult.hasErrors()) {
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return "formUser";
		}
		userRepository.save(user);
		response.setStatus(HttpServletResponse.SC_CREATED);
		return "redirect:/success";
	}

}
